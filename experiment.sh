#!/bin/bash

function print_time {
cat << EOF > $1
{
  "human_readable": "`date`",
  "json": "`date +%y-%m-%dT%H:%M:%SZ`",
  "machine_readable": `date +%s`
}
EOF
}

input_folder=$PWD

# 1. create folder
# node detection:
stamp="$(date +%Y%m%d_%H%M%S)-$EXPERIMENT_NAME-$(hostname)"
if [[ "$FOLDER_POSTFIX" != "" ]];
then
  stamp="$stamp-$FOLDER_POSTFIX"
fi
foldername="$PWD/experiments/$stamp"
mkdir -p $foldername

# link last experiment
cd experiments
rm last
ln -s $stamp last

# 2. copy input files in folder name. To keep paths consistent do this from the input folder
cd $input_folder
for input_file in $INPUT_FILES;
do
  cp -L -r $input_file $foldername  # also folders (-r) and follow symlinks (-L)
  if [ "$(basename $input_file)" == "CMakeCache.txt" ];
  then
    project_name=`cat $input_file | grep CMAKE_PROJECT_NAME | cut -d '=' -f 2`
    mv $foldername/CMakeCache.txt $foldername/CMakeCache.txt.$project_name
  fi
done

# 3. Save state of used software
# me
cp $0 $foldername

# that is the line where we assume that the git repository is in path!
me=`which experiment.sh`
if [[ "$me" == "" ]];
then
  echo could not store the experiment.sh script and its git repository!
else
  cp $me $foldername
  GIT_REPOS="$GIT_REPOS `dirname $me`"
fi

# Store machine information
hostname > $foldername/hostname
uname -r > $foldername/uname-r
cp /proc/meminfo $foldername
cp /proc/cpuinfo $foldername

# Store env
env > $foldername/env

# Store modules
module list &> $foldername/modules_list

# Git repos
# REM: sometimes the git nodule needs to be load to have access to git on the compute nodes
for git_repo in $GIT_REPOS;
do
  echo --------------------- >> $foldername/git_repos
  echo $git_repo : >> $foldername/git_repos
  cat $git_repo/.git/`cat $git_repo/.git/HEAD | cut -d ' ' -f 2` >> $foldername/git_repos
  cd $git_repo
  git diff > $foldername/`basename $git_repo`.diff
  cd $input_folder
done

# 4. cd to foldername and rewrite timestep in it for convenience.
cd $foldername
print_time started.json

# 5. Run the experiment, piping parameters
run $@

cd $foldername

# 6. Zip all input and output files but the json and csv stuff...
# TODO

# 7. Create file list
find . > filelist

# 8. create finished.json file
print_time finished.json






