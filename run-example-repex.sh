#!/bin/bash


# to be changed by the user:

export EXPERIMENT_NAME=ExampleExperiment

# can be used for hyperparameters...
export FOLDER_POSTFIX=A

# space seperated list. if one path is a folder all it's content is copied.
# absolute and relative paths are allowed.
# CMakeCache.txt file well be renamed into CMakeCache.txt.PROJECTNAME
export INPUT_FILES="$HOME/.bashrc test-folder/ CMakeCache.txt"

# git repositories to save:
# absolute and relative paths to the git repositories root dir are allowed.
# a git diff and the checked out git revision hash will get stored.
export GIT_REPOS="$HOME/workspace/wg-plan"


function run {
  # script that runs your experiment goes here
  # it can make use of command line parameters! as $1...N

  echo doing experiment... > output.txt
  sleep 3
  # do something with the input files...
  head .bashrc >> output.txt
  echo finished experiment >> output.txt
}

# don't forget to export  your function
export -f run

# if source line fails install the git repo to your path in .bashrc!!
source experiment.sh
