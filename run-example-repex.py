#!/usr/bin/env python3
import repex
import os


# to be changed by the user:

EXPERIMENT_NAME="ExampleExperiment"

# can be used for hyperparameters...
FOLDER_POSTFIX="A"

# space seperated list. if one path is a folder all it's content is copied.
# absolute and relative paths are allowed.
# CMakeCache.txt file well be renamed into CMakeCache.txt.PROJECTNAME
INPUT_FILES=[os.getenv('HOME') + "/.bashrc", "test-folder/", "CMakeCache.txt"]

# git repositories to save:
# absolute and relative paths to the git repositories root dir are allowed.
# a git diff and the checked out git revision hash will get stored.
GIT_REPOS=[os.getenv("HOME") + "/workspace/wg-plan"]


def run():
    # script that runs your experiment goes here
    # this function gets executed in the experiment directory after
    # copying all the input files in it, along with the git repository version hashes and
    # diffs. The foldername contains the experiment name, the hostname, a timestamp and
    # the folder postfix. Since every experiment is executed in its own folder
    # (a subfolder of experiments, you can also run multiple experiments at the same time)
    # it can make use of command line parameters! as (sys.argv...)

    os.system('echo doing experiment... > output.txt')
    os.system('sleep 3')
    # do something with the input files...
    os.system('head .bashrc >> output.txt')
    os.system('echo finished experiment >> output.txt')

repex.run(EXPERIMENT_NAME=EXPERIMENT_NAME, FOLDER_POSTFIX=FOLDER_POSTFIX,
        GIT_REPOS=GIT_REPOS, INPUT_FILES=INPUT_FILES, experiment_function=run)
