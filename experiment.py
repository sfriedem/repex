# Python clone of the famous repex library

import os
import datetime
import json
import socket
import shutil
import sys



def run(EXPERIMENT_NAME='exp', FOLDER_POSTFIX='', INPUT_FILES=[], GIT_REPOS=[],
        experiment_function=None):
    print("Running %s with repex..." % __file__)
    def print_time(filename):
        now = datetime.datetime.now()
        out = {
                "human_readable": str(now),
                "json": "%sZ" % now.isoformat(),
                "machine_readable": int(now.timestamp())
                }
        with open(filename, 'w+') as f:
            json.dump(out, f)


    input_folder=os.getcwd()

    # 1. create folder
    now = datetime.datetime.now()
    stamp="%02d%02d%02d_%02d%02d%02d-%s-%s" % (now.year, now.month, now.day, now.hour, now.minute,
        now.second, EXPERIMENT_NAME, socket.gethostname())

    if FOLDER_POSTFIX != '':
        stamp += '-' + FOLDER_POSTFIX

    foldername=os.getcwd() + "/experiments/" + stamp
    os.makedirs(foldername)

    # link last experiment
    os.chdir("experiments")
    try:
        if os.path.islink("last"):
            os.remove("last")
        os.symlink(stamp, "last")
    except:
        print("Error putting the last link! probably another repex experiment is running at the same time.")
        print("So not setting the last linkk for now.")

    # 2. copy input files in folder name. To keep paths consistent do this from the input folder
    os.chdir(input_folder)
    for input_file in INPUT_FILES:
        os.system('cp -L -r %s %s' % (input_file, foldername))  # also folders (-r) and follow symlinks (-L)... there is no faster way than doing it like this!
        bn = os.path.basename(input_file)
        if bn == "CMakeCache.txt":
            new_name = ''
            with open(input_file) as f:
                for line in f.readlines():
                    if 'CMAKE_PROJECT_NAME' in line:
                        new_name = line.split("=")[1][:-1]
                        break

            assert new_name != ''
            os.rename(foldername + "/" + bn, "%s/CMakeCache.txt.%s"
                    % (foldername,new_name))

    # 3. Save state of used software
    # me
    shutil.copy(sys.argv[0], foldername+"/" + os.path.basename(sys.argv[0]))

    # experiment.py
    shutil.copy(__file__, foldername+"/" + os.path.basename(__file__))
    GIT_REPOS.append(os.path.dirname(__file__))

    # Store machine information
    os.system('hostname > %s/hostname' % foldername)
    os.system('uname -r > %s/uname-r' % foldername)
    shutil.copy('/proc/meminfo', foldername + '/meminfo')
    shutil.copy('/proc/cpuinfo', foldername + '/cpuinfo')

    # Store env
    os.system('env > %s/env' % foldername)

    # Store modules
    os.system('module list &> %s/modules_list' % foldername)

    # Git repos
    # REM: sometimes the git nodule needs to be load to have access to git on the compute nodes
    if len(GIT_REPOS) > 0:
        assert shutil.which('git') is not None  # git: module loaded / installed?
    for git_repo in GIT_REPOS:
        print('Copying repo', git_repo)
        with open(foldername + '/git_repos', 'a+') as f:
            f.write("""---------------------
%s :
""" % git_repo)

        os.system("cat %s/.git/`cat %s/.git/HEAD | cut -d ' ' -f 2` >> %s/git_repos" %
            (git_repo, git_repo, foldername))

        os.chdir(git_repo)
        os.system('git diff > %s/`basename %s`.diff' % (foldername, git_repo))
        os.chdir(input_folder)

    # 4. cd to foldername and rewrite timestep in it for convenience.
    os.chdir(foldername)
    print_time('started.json')

    # 5. Run the experiment, piping parameters
    experiment_function()

    os.chdir(foldername)

    # 6. Zip all input and output files but the json and csv stuff...
    # TODO

    # 7. Create file list
    os.system('find . > filelist')

    # 8. create finished.json file
    print_time('finished.json')
